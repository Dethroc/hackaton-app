import VueI18n from 'vue-i18n'
import messages from 'src/i18n'

let i18n

export default ({ app, Vue }) => {
  Vue.use(VueI18n)

  app.i18n = new VueI18n({
    locale: 'pt-br',
    fallbackLocale: 'en-us',
    messages
  })

  // Set i18n instance on app
  i18n = app.i18n
}

export {
  i18n
}
