import Money from 'src/helpers/Money'

// leave the export, even if you don't use it
export default ({ Vue }) => {
  Vue.prototype.$money = new Money()
}
