import axios from 'axios'

const axiosApi = axios.create({
  baseURL: 'http://9637d6f5.ngrok.io'
})

export default ({ Vue }) => {
  Vue.prototype.$axios = axios
  Vue.prototype.$axiosApi = axiosApi
}

export { axios, axiosApi }
