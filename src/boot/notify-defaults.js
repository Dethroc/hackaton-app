import { Notify } from 'quasar'

Notify.setDefaults({
  icon: 'mdi-check-circle',
  color: 'white',
  position: 'bottom',
  textColor: 'positive',
  actions: [
    { icon: 'mdi-close', color: 'black' }
  ]
})
