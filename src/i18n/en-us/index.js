// This is just an example,
// so you can safely delete all default props below

export default {
  buttons: {
    access: 'Enter Account',
    login: 'Login',
    register: 'Register',
    reset: 'Reset',
    confirm: 'Confirm',
    save: 'Save',
    remove: 'Remove',
    add: 'Add',
    cancel: 'Cancel',
    close: 'Close'
  },
  labels: {
    email: 'Email',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    remember: 'Remember me',
    name: 'Name',
    lastname: 'Lastname'
  },
  anchors: {
    register: 'Register',
    login: 'Login',
    firstAccess: 'First Access',
    companyCode: 'Company Code',
    accountRegistered: 'Account Registered'
  },
  titles: {
    dashboard: 'Dashboard',
    transactions: 'Transactions',
    accounts: 'Accounts',
    profile: 'Profile',
    settings: 'Settings',
    creditCards: 'Credit Cards',
    groups: 'Groups',
    groupSettings: 'Group Settings',
    groupInsert: 'Insert Group',
    expenseInsert: 'Insert Expense'
  },
  modals: {
    profile: {
      edit: 'Edit Informations'
    },
    groups: {
      create: 'Create Group',
      createExpense: 'Create Expense'
    }
  },
  alerts: {
    created: 'Register inserted with success.',
    updated: 'Register updated with success.',
    deleted: 'Register deleted with success.',
    validationFails: 'Please review fields again.'
  },
  containers: {
    empty: 'Data not found.'
  },
  questions: {
    delete: 'Wish to remove this record?'
  },
  groups: {
    types: {
      private: 'Private',
      shared: 'Shared',
      personal: 'Personal'
    }
  },
  categories: {
    home: 'Home',
    heart: 'Heart',
    cart: 'Grocery Store'
  }
}
