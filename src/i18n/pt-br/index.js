// This is just an example,
// so you can safely delete all default props below

export default {
  buttons: {
    access: 'Acessar Conta',
    login: 'Entrar',
    register: 'Registre-se',
    reset: 'Limpar',
    confirm: 'Confirmar',
    save: 'Salvar',
    remove: 'Excluir',
    add: 'Adicionar',
    cancel: 'Cancelar',
    close: 'Fechar',
    edit: 'Editar',
    previous: 'Voltar',
    confirmOffer: 'Confirmar oferta'
  },
  labels: {
    email: 'E-mail',
    password: 'Senha',
    confirmPassword: 'Confirmar Senha',
    remember: 'Lembrar-me',
    name: 'Nome',
    lastname: 'Sobrenome',
    company: {
      name: 'Nome da Empresa',
      document: 'CNPJ',
      zip: 'Cep',
      address: 'Endereço',
      number: 'Número',
      complement: 'Complemento',
      neighboor: 'Bairro',
      city: 'Cidade',
      state: 'Estado'
    },
    personal: {
      name: 'Nome de Cadastro',
      email: 'E-mail corporativo'
    }
  },
  anchors: {
    register: 'Cadastrar empresa',
    login: 'Entrar',
    accountRegistered: 'Já possuo conta',
    previous: 'Voltar'
  },
  titles: {
    dashboard: 'Dashboard',
    transactions: 'Transações',
    accounts: 'Contas',
    profile: 'Perfil',
    settings: 'Configurações',
    creditCards: 'Cartões de Crédito',
    groups: 'Grupos',
    groupSettings: 'Configurações de Grupo',
    groupInsert: 'Inserir Grupo',
    expenseInsert: 'Inserir Despesa'
  },
  modals: {
    profile: {
      edit: 'Editar Informações'
    },
    groups: {
      create: 'Inserir Grupo',
      createExpense: 'Inserir lançamento'
    },
    rides: {
      offer: 'Oferecer carona',
      request: 'Solicitar carona'
    }
  },
  alerts: {
    created: 'Registro inserido com sucesso.',
    updated: 'Registro atualizado com sucesso.',
    deleted: 'Registro excluído sucesso.',
    validationFails: 'Por favor, verifique os campos novamente.',
    welcome: 'Bem-vindo, usuário'
  },
  containers: {
    empty: 'Nenhum registro encontrado.'
  },
  questions: {
    remove: 'Gostaria de excluír este item?'
  },
  groups: {
    private: 'Privado',
    shared: 'Compartilhado',
    personal: 'Pessoal'
  },
  categories: {
    home: 'Casa',
    heart: 'Saúde',
    cart: 'Mercado'
  }
}
