import { date } from 'quasar'

const range = 1

export default class Timer {
  static generateMonths () {
    let newDate = new Date(Date.now())

    let submonths = []

    let months = []

    for (let i = 0; i <= range; i++) {
      submonths.push(date.subtractFromDate(newDate, { month: i }))
    }

    submonths.reverse()

    for (let i = 1; i <= range; i++) {
      months.push(date.addToDate(newDate, { month: i }))
    }

    let arr = []

    let done = arr.concat(submonths, months)

    return done
  }

  static generateExpenses () {
    //
  }

  static format (value, template = 'MMM YY') {
    return date.formatDate(value, template)
  }
}
