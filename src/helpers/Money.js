export default class Money {
  format (value, format = 'pt-BR') {
    if (typeof value === 'string') {
      value = parseInt(value)
    }

    return (value / 100).toLocaleString(format, {
      style: 'currency',
      currency: 'BRL',
      currencyDisplay: 'symbol'
    })
  }

  transform (value) {
    return parseInt(value.replace(/\D/g, ''))
  }
}
