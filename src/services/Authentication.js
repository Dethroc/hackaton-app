'use strict'

import { axiosApi } from 'boot/axios'

/**
 * Authentication requests / responses
 */
export default class Authentication {
  login (credentials, callback) {
    axiosApi.post('/api/login', credentials)
      .then((response) => {
        callback(response.data.access_token)
      })
      .catch(() => {
        callback(null)
      })
  }

  register (credentials) {
    // ...
  }

  changePassword (credentials) {
    // ...
  }

  recovery (credentials) {
    // ...
  }
}
