'use strict'

import { LocalStorage } from 'quasar'

const key = 'profile'

export default class Profile {
  static retrieve (callback) {
    /**
     * Axios request, retrieve user informations.
     * Store the userID to LocalStorage.
     * Return user informations to callback.
     */

    let error = null

    callback(error, { data: LocalStorage.getItem(key) })
  }

  static update (values, callback) {
    /**
     * Axios Request
     */

    let error = null

    callback(error, { data: LocalStorage.set(key, values) })
  }

  static remove () {
    return LocalStorage.remove(key)
  }
}
