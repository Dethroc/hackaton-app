/**
 * @param {*} state
 */
export const informations = state => state.informations

/**
 * @param {*} state
 */
export const incomes = state => state.incomes
