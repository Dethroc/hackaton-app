'use strict'

import Profile from 'src/services/Profile'

export const retrieve = ({ commit }) => Profile.retrieve((err, response) => {
  if (err) {
    console.log(err)
  }

  commit('retrieve', response.data)
})

export const update = ({ commit }, informations) => {
  delete informations.remember

  Profile.update(informations, (err, response) => {
    if (err) {
      console.log(err)
    }

    commit('update', informations)
  })
}
