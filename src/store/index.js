import Vue from 'vue'
import Vuex from 'vuex'

import general from './general'
import profile from './profile'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      general,
      profile
    }
  })

  return Store
}
