export const setTitle = ({ commit }, title) => {
  commit('setTitle', title)
}
