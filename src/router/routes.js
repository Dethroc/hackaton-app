
const routes = [
  {
    path: '/',
    redirect: 'panel'
  },
  {
    path: '/auth',
    component: () => import('layouts/auth/Main.vue'),
    redirect: 'auth/login',
    meta: { guest: true },
    children: [
      { path: 'login', component: () => import('pages/auth/Login.vue') },
      { path: 'register', component: () => import('pages/auth/Register.vue') }
    ]
  },
  {
    path: '/panel',
    component: () => import('layouts/panel/Main.vue'),
    redirect: 'panel/dashboard',
    meta: { authenticated: true },
    children: [
      {
        name: 'dashboard',
        path: 'dashboard',
        component: () => import('pages/panel/dashboard/Main.vue')
      },
      {
        name: 'profile',
        path: 'profile',
        component: () => import('pages/panel/profile/Main.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/general/Error404.vue')
  })
}

export default routes
