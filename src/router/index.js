import Vue from 'vue'
import VueRouter from 'vue-router'
import { LocalStorage } from 'quasar'

import routes from './routes'

Vue.use(VueRouter)

const Router = new VueRouter({
  scrollBehavior: () => ({ y: 0 }),
  routes,
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE
})

Router.beforeEach((to, from, next) => {
  //  NOTE: Se a rota requer autenticação e eu NÃO estou logado (não possuo um token de acesso)
  //  Redireciona para tela de login. (Evita usuários não logados de acessar o painel)
  switch (true) {
    case to.matched.some(record => record.meta.authenticated) && !LocalStorage.has('token'):
      next({
        path: '/auth'
      })
      break

    case to.matched.some(record => record.meta.guest) && LocalStorage.has('token'):
      next({
        path: '/panel'
      })
      break

    default:
      next()
      break
  }
})

export default Router
