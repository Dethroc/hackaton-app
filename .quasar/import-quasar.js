/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 *
 * You are probably looking on adding startup/initialization code.
 * Use "quasar new boot <name>" and add it there.
 * One boot file per concern. Then reference the file(s) in quasar.conf.js > boot:
 * boot: ['file', ...] // do not add ".js" extension to it.
 *
 * Boot files are your "main.js"
 **/

import lang from 'quasar/lang/pt-br'


import Vue from 'vue'

import {Quasar,QAvatar,QDialog,QLayout,QPageContainer,QHeader,QFooter,QDrawer,QScrollArea,QImg,QPage,QToolbar,QToolbarTitle,QBtn,QBtnGroup,QIcon,QList,QItem,QItemSection,QItemLabel,QSeparator,QField,QInput,QForm,QDate,QPopupProxy,QSelect,QToggle,QCheckbox,QRadio,QTabs,QTab,QRouteTab,QTabPanels,QTabPanel,QCard,QCardSection,QCardActions,QPageSticky,QStepper,QStep,QTime,Ripple,ClosePopup,Notify,Screen,LocalStorage,Dialog} from 'quasar'


Vue.use(Quasar, { config: {},lang: lang,components: {QAvatar,QDialog,QLayout,QPageContainer,QHeader,QFooter,QDrawer,QScrollArea,QImg,QPage,QToolbar,QToolbarTitle,QBtn,QBtnGroup,QIcon,QList,QItem,QItemSection,QItemLabel,QSeparator,QField,QInput,QForm,QDate,QPopupProxy,QSelect,QToggle,QCheckbox,QRadio,QTabs,QTab,QRouteTab,QTabPanels,QTabPanel,QCard,QCardSection,QCardActions,QPageSticky,QStepper,QStep,QTime},directives: {Ripple,ClosePopup},plugins: {Notify,Screen,LocalStorage,Dialog} })
